//read frames and send over serial
#include <LiquidCrystal.h>
#include <Wire.h>
#include <Dwenguino.h>


const int N_INTERFLAGFRAMES = 50; // N frames between flag, flag used to synchronize Serial with pc
const int SENSORID = 25;
const int LEN_ROW = 5;
const int LEN_COLUMN = 5;
//const int N_taxels = LEN_ROW*LEN_COLUMN;
volatile uint8_t FlagCount =0;

byte received_data[LEN_ROW][LEN_COLUMN];

unsigned int BAUD_PRESCALLER = 25; //0 for 1MHz



unsigned long time0;
unsigned long time1;
unsigned long time2;
unsigned long time3;
unsigned long time4;
unsigned long time5;
unsigned long time6;
unsigned long time7;
unsigned long time8;
unsigned long time9;


void setup() {
  initDwenguino();
  dwenguinoLCD.clear();
  dwenguinoLCD.print("USART sensor ");
  dwenguinoLCD.print(SENSORID);
  
  //// setup Serial connection to host
  //Serial.begin(256000,SERIAL_8N1);
  Serial.begin(9600);
  delay(1000);
  Serial.flush();

  //// setup USART to sensor
    /* Set baud rate */
   UBRR1H = (uint8_t)(BAUD_PRESCALLER>>8);
   UBRR1L = (uint8_t)(BAUD_PRESCALLER);
   /* Enable receiver and transmitter */
   UCSR1B = (1<<RXEN1)|(1<<TXEN1);
  /* Set frame format: 8data, 2stop bit */
  UCSR1C = (1<<USBS1)|(3<<UCSZ10);
  /*set as synchronous USART*/
  UCSR1C |= (1<<UMSEL10);
  UCSR1C &= ~(1<<UMSEL11);
  /*set XCK1 pin as intput (Slave mode, internal clock used) */
  DDRD &= ~(1<<PD5);

  // init variabelen matrixen
    for (int k = 0; k < LEN_COLUMN; k++) {
      for (int j = 0; j < LEN_ROW; j++) {
        received_data[j][k] = 66;
      }
    }
}

void loop() {
  time0 = millis();
  wait_for_start();
  time1 = millis();
  Serial.println("wait took:");
  Serial.println(time1-time0);
  
  time2 = millis();
  read_frame();
  time3 = millis();
  Serial.println("read took");
  Serial.println(time3-time2);
  
  time4 = millis();
  serialWriteFlag(SENSORID);
  time5 = millis();
  Serial.println("write flag took::");
  Serial.println(time5-time4);
  
  time6 = millis();
  serialWriteMatrix();
  time7 = millis();
  Serial.println("write matrix took:");
  Serial.println(time7-time6);
}

void serialWriteFlag(uint8_t sensorID){
  FlagCount+=1;
  if (FlagCount == N_INTERFLAGFRAMES){
    Serial.write(99);
    Serial.write(33);
    Serial.write(66);
    Serial.write(N_INTERFLAGFRAMES);
    Serial.write(sensorID);
    FlagCount=0;
  }
}

void serialWriteMatrix() {
  
  for (int j = 0; j < LEN_ROW; j++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      Serial.write(received_data[j][k]);
      //uint8_t c =77;
      //Serial.write(c);
    }
  }
}

void wait_for_start(){
  byte b = 1;
  //Serial.println(b);
  while (1){
    time8 = millis();
    byte b = USART_receive();
    //Serial.println(b);
    time9 = millis();
    Serial.println(time9-time8);
    if (b==252){
      break;
    }
  }
  //Serial.println("out of while");
  return;
}

void read_frame(){

    for (int j = 0; j < LEN_ROW; j++) { // iter rows
      for (int k = 0; k < LEN_COLUMN; k++) { // iter columns 
        byte v = USART_receive();     // receive pixel value
        //Serial.print(v);
        received_data[j][k] = v;
      }
    }
}
 
unsigned char USART_receive(void){
 
 while(!(UCSR1A & (1<<RXC1)));
 return UDR1;
 
}
 
void USART_send( unsigned char data){
 
   while(!(UCSR1A & (1<<UDRE1)));
   UDR1 = data;
   
}
 
void USART_putstring(char* StringPtr){
 
  while(*StringPtr != 0x00){
     USART_send(*StringPtr);
     StringPtr++;}
 
}

