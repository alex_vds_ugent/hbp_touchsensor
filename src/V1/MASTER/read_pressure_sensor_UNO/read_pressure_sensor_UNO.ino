
volatile int buff[] = {666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666};
const int LEN_BUFF = sizeof(buff) / sizeof(int);

unsigned long time0;
unsigned long time1;
unsigned long time2;
unsigned long time3;

// the setup routine runs once when you press reset:
void setup() {
  // supply 5V (5V pin obstructed)
  pinMode(52, OUTPUT);
  digitalWrite(52, HIGH);
  // initialize serial communication at 9600 bits per second:
  
  Serial.begin(9600);
}
 
// the loop routine runs over and over again forever:
void loop() {

  /*
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  for (int i=LEN_BUFF-1;i>0;i--){
    buff[i] = buff[i-1];
  }
  buff[0] = sensorValue;

  int count = 0;
  for (int i=0; i<LEN_BUFF; i++){
    count+=buff[i];
  }
  int avg = count/LEN_BUFF;
  
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue * (5.0 / 1023.0);
  // print out the value you read:
  Serial.print(voltage);
  Serial.print("V, ");
  Serial.print(sensorValue);
  Serial.print(", avg: ");
  Serial.println(avg);
  */
  time0 = micros();
  float avg = get_avg(60);
  time1 = micros();
  
  Serial.print(avg);
  Serial.print("V, ");
  Serial.print("get_avg took (us): ");
  Serial.println(time1-time0);
}

float get_avg(int N){

  int sum =0;
  for (int i=0; i<N; i++){
    sum += analogRead(A0);
  }
  
  int avg = sum/N;
  float voltage = avg * (5.0 / 1023.0);

  return voltage;
}

