//tested on ubuntu_Dwenguino_arduinoIDE1.8.5_usbASP
//tested on ubuntu_ArduinoMega2560_arduinoIDE1.8.5

//there is significant overhead with each I2C master-slave interaction using the wire library. It turns out (empirically) that only 15 bytes can be sent per I2C interaction (due to the wire library, not I2C). 
//Here, the 5x5 matrix is sent in two I2C interactions.  

#include <LiquidCrystal.h>
#include <Wire.h>
//#include <Dwenguino.h>


const int ADRESS_ARRAY[] = {38}; //30 35 
const int LEN_ADRESS_ARRAY = sizeof(ADRESS_ARRAY) / sizeof(int);
const int LEN_ROW = 5;
const int LEN_COLUMN = 5;
const int N_taxels = LEN_ROW*LEN_COLUMN;
int received_data[LEN_ADRESS_ARRAY][LEN_ROW][LEN_COLUMN];

unsigned long time0;
unsigned long time1;
unsigned long time2;
unsigned long time3;
unsigned long time4;
unsigned long time5;
unsigned long time6;
unsigned long time7;
unsigned long time8;
unsigned long time9;

void setup() {
  //initDwenguino();
  //pinMode(13,OUTPUT); // for debugging
  Serial.begin(9600);
  delay(10);
  Serial.println("Flexsensor test initialisation");

/*  // init variabelen matrixen
  for (int i = 0; i < LEN_ADRESS_ARRAY; i++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      for (int j = 0; j < LEN_ROW; j++) {
        received_data[i][j][k] = 1;
      }
    }
  }*/
  
  Wire.begin();
  delay(500);
  Wire.setClock(300000);
  //TWBR = 10;
}

void loop() {

  //Serial.print( "1, TWBR = ");
  //Serial.println( TWBR);  

  //Serial.print( "TWSR = ");
  //Serial.println(TWSR);    
  //digitalWrite(13,HIGH);
  time0 = millis();
  pullData();  //pull data from sensors in aadress array
  time1 = millis();
  //digitalWrite(13,LOW);

  Serial.print("PullData took (ms): ");
  Serial.println(time1-time0);
  Serial.print("MOSI write first byte took (us): ");
  Serial.println(time3-time2);
  Serial.print("MOSI read 15 taxels took (us): ");
  Serial.println(time4-time3);
  Serial.print("MOSI write second byte took (us): ");
  Serial.println(time6-time5);
  Serial.print("MOSI read 10 taxels took (us): ");
  Serial.println(time7-time6);

  time8 = micros();
  printMatrix(0);
  time9 = micros();
  Serial.print("Serial write took (us): ");
  Serial.println(time9-time8);

  delay(100);
}


void printMatrix(int sensor) {
  for (int j = 0; j < LEN_ROW; j++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      Serial.print(received_data[sensor][j][k]);
      Serial.print(",");
    }
    Serial.println("");
  }

  Serial.println("");
}

void pullData() {

  // iter sensors
  for (int i = 0; i < LEN_ADRESS_ARRAY; i++) {
    //request data matrix
    //select row
    
    time2 = micros();
    
    Wire.beginTransmission(ADRESS_ARRAY[i]);
    Wire.write(0);
    Wire.endTransmission();
    
    time3 = micros();

    Wire.requestFrom(ADRESS_ARRAY[i], 15);//N_taxels

    time4 = micros();
    
    for (int j = 0; j < 3; j++) { // iter rows
      for (int k = 0; k < LEN_COLUMN; k++) { // iter columns 
        int v = Wire.read();     // receive pixel value
        //Serial.print(v);
        received_data[i][j][k] = v;

      }
    }

    time5 = micros();
    
    Wire.beginTransmission(ADRESS_ARRAY[i]);
    Wire.write(1);
    Wire.endTransmission();

    time6 = micros();
    
    Wire.requestFrom(ADRESS_ARRAY[i], 10);//N_taxels

    
        
    for (int j = 3; j < LEN_ROW; j++) { // iter rows
      for (int k = 0; k < LEN_COLUMN; k++) { // iter columns 
        int v = Wire.read();     // receive pixel value
        //Serial.print(v);
        received_data[i][j][k] = v;

      }
    }
    time7 = micros();
    

    
  }
}

