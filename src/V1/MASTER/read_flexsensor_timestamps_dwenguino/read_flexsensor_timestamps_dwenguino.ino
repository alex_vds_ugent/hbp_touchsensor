//tested on ubuntu_Dwenguino_arduinoIDE1.8.5_usbASP
#include <LiquidCrystal.h>
#include <Wire.h>
#include <Dwenguino.h>


const int ADRESS_ARRAY[] = {66}; //30 35 
const int LEN_ADRESS_ARRAY = sizeof(ADRESS_ARRAY) / sizeof(int);
const int LEN_ROW = 5;
const int LEN_COLUMN = 5;

int received_data[LEN_ADRESS_ARRAY][LEN_ROW][LEN_COLUMN];

void setup() {
  initDwenguino();
  pinMode(13,OUTPUT); // for debugging
  Serial.begin(9600);
  delay(10);
  Serial.println("Flexsensor test initialisation");
/*  // init variabelen matrixen
  for (int i = 0; i < LEN_ADRESS_ARRAY; i++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      for (int j = 0; j < LEN_ROW; j++) {
        received_data[i][j][k] = 1;
      }
    }
  }*/
  Wire.begin();
}

void loop() {
  digitalWrite(13,HIGH);
  pullData();  //pull data from sensors in aadress array
  digitalWrite(13,LOW);
  printMatrix(0);    
  printMatrix_ints(0);
/*  for (int k = 0; k < LEN_ADRESS_ARRAY; k++) {
    Serial.print("Data sensor: ");
    Serial.println(ADRESS_ARRAY[k]);
    printMatrix(k);

  }*/
  delay(300);
}


void printMatrix(int sensor) {
  for (int j = 0; j < LEN_ROW; j++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      Serial.print(received_data[sensor][j][k]);
      Serial.print(",");
    }
    Serial.println("");
  }

  Serial.println("");
}

void printMatrix_ints(int sensor) {
  for (int j = 0; j < LEN_ROW; j++) {
    unsigned long int myint = 0;
    for (int k = 0; k < LEN_COLUMN; k++) {
      unsigned long int val = received_data[sensor][j][k];
      //Serial.print("received: ");
      //Serial.print(val);
      val = val << 8*k;
      //Serial.print(", interpreted as: ");
      //Serial.print(val);
      myint = myint+val;
      //Serial.print(", updated myint: ");
      //Serial.println(myint);
    }
    Serial.print(myint);
    Serial.print(",");
    Serial.println("");
  }

  Serial.println("");
}


void pullData() {
  // iter sensors
  for (int i = 0; i < LEN_ADRESS_ARRAY; i++) {
    // iter rows
    for (int j = 0; j < LEN_ROW; j++) {
      //select row
      Wire.beginTransmission(ADRESS_ARRAY[i]);
      Wire.write(j);
      Wire.endTransmission();

      //get single row data
      Wire.requestFrom(ADRESS_ARRAY[i], LEN_COLUMN);
      for (int k = 0; k < LEN_COLUMN; k++) {
        int v = Wire.read();     // receive pixel value
        received_data[i][j][k] = v;

      }
    }
  }
  /*
  // print
  for (int i = 0; i < LEN_ADRESS_ARRAY; i++) {
    for (int j = 0; j < LEN_ROW; j++) {
      for (int k = 0; k < LEN_COLUMN; k++) {
        Serial.print(received_data[i][j][k]);
        Serial.print(",");
      }
      Serial.println("");
    }
    Serial.println("");
  }
  */
}

