
unsigned int servo_delay_ms = 50;//should be multiple of 10
unsigned int MIN_POS = 25;
unsigned int MAX_POS = 150;
volatile unsigned int servo_val = 26;
volatile unsigned char ASCEND = 1;
volatile unsigned char count = 0;
unsigned char META_TIMER;

#include <Servo.h>

Servo myservo;  // create servo object to control a servo

void setup() {

  Serial.begin(9600); // for printing
  myservo.attach(2);
  delay(500);

  //stop interrupts
  cli();
  //set timer0 interrupt at 2kHz
  TCCR0A = 0;// set entire TCCR0A register to 0
  TCCR0B = 0;// same for TCCR0B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 100hz interupts
  OCR0A = 156; 
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS02 and CS00 bits for 1024 prescaler
  TCCR0B |= (1 << CS02) | (1 << CS00);   
  // enable timer compare interrupt
  TIMSK0 |= (1 << OCIE0A);
  //allow interrupts
  sei();

  META_TIMER = servo_delay_ms/10;
}
                                                                 

void loop() {
  /*
  for (int i=25;i<150;i++){
    myservo.write(i);
    delay(50);
  }

  for (int i=150;i>25;i--){
    myservo.write(i);
    delay(50);
  }
  //myservo.write(90);                  // sets the servo position according to the scaled value (value between 0 and 180)
  //delay(15); 
  */
}

void set_servo(unsigned char val){
  Serial.println(val);
  if (val<MIN_POS){
    myservo.write(MIN_POS);
  }
  else if (val>MAX_POS){
    myservo.write(MAX_POS);
  }
  else {
    myservo.write(val);
  }
}

ISR(TIMER0_COMPA_vect){
  if (count==META_TIMER){
    count =0;
    if (ASCEND){
      servo_val++;
      set_servo(servo_val);
    }
    else {
     servo_val--;
     set_servo(servo_val);
    }
  
    if (servo_val==MAX_POS){
      ASCEND=0;      
    }
    if (servo_val==MIN_POS){
      ASCEND=1;      
    }
  }
  else {
    count++;
  }
}

