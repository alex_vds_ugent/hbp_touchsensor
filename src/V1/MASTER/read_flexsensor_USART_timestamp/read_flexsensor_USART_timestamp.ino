//tested on ubuntu_Dwenguino_arduinoIDE1.8.5

//#include <LiquidCrystal.h>
//#include <Wire.h>
//#include <Dwenguino.h> //not vital


const int ADRESS_ARRAY[] = {33}; //30 35 
const int LEN_ADRESS_ARRAY = sizeof(ADRESS_ARRAY) / sizeof(int);
const int LEN_ROW = 5;
const int LEN_COLUMN = 5;
const int N_taxels = LEN_ROW*LEN_COLUMN;

int received_data[LEN_ADRESS_ARRAY][LEN_ROW][LEN_COLUMN];

unsigned char fromTiny; 

unsigned int BAUD_PRESCALLER = 0; //0 for 1MHz

unsigned long time0;
unsigned long time1;
unsigned long time2;
unsigned long time3;
unsigned long read_time;
unsigned long count =0;

unsigned char taxel_count = 0;

void setup() {

  Serial.begin(9600);
  delay(10);
  Serial.println("Flexsensor test initialisation");

  /* set Global Interrupt Enable */
  sei(); 


  /* USART init */
  /* Set baud rate */
   UBRR1H = (uint8_t)(BAUD_PRESCALLER>>8);
   UBRR1L = (uint8_t)(BAUD_PRESCALLER);
   /* Enable receiver and transmitter */
   UCSR1B = (1<<RXEN1)|(1<<TXEN1);
   /* Enable on receive interupt*/
   UCSR1B |= (1<<RXCIE1);
  /* Set frame format: 8data, 2stop bit */
  UCSR1C = (1<<USBS1)|(3<<UCSZ10);
  /*set as synchronous USART*/
  UCSR1C |= (1<<UMSEL10);
  UCSR1C &= ~(1<<UMSEL11);

  /*set XCK1 pin as intput (Slave mode, internal clock used) */
  DDRD &= ~(1<<PD5);
  
}

void loop() {
  print_frame(0);
  //Serial.print("receiving last frame took (us): ");
  //Serial.println(read_time);
  delay(8);
  
  /*Serial.println("lets listen");
  
  read_frame();
  time1 = millis();
  print_frame(0);


  Serial.print("read first taxel took (us): ");
  Serial.println(time3-time2);
  */
  //Serial.print("store taxel took (us): ");
  //Serial.println(time4-time3);
  //fromTiny = USART_receive();
  //Serial.println(fromTiny);
  //delay(100);
}


void print_frame(int sensor) {
  for (int j = 0; j < LEN_ROW; j++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      Serial.print(received_data[sensor][j][k]);
      Serial.print(",");
    }
    Serial.println("");
  }

  Serial.println("");
}

ISR(USART1_RX_vect)
{
  if (taxel_count==0){
    time0 = micros();
  }
  int j=taxel_count/5;
  int k=taxel_count-(j*5);
  received_data[0][j][k] = UDR1;
  
  if (taxel_count<24){
    taxel_count+=1;
  }
  else {
    read_time = micros()-time0;
    taxel_count=0;
  }
}

void read_frame(){
    
    for (int j = 0; j < LEN_ROW; j++) { // iter rows
      for (int k = 0; k < LEN_COLUMN; k++) { // iter columns 
        
        time2 = micros();    
        byte v = USART_receive();     // receive pixel value
        time3 = micros();    
        
        //Serial.print(v);
        received_data[0][j][k] = time3-time2;
        
      }
    }
}
 
unsigned char USART_receive(void){
 count=0;
 //while(!(UCSR1A & (1<<RXC1)));
 while(!(UCSR1A & (1<<RXC1))){
  count+=1;
 }
 Serial.println(count);
 return UDR1;
 
}
 
void USART_send( unsigned char data){
 
   while(!(UCSR1A & (1<<UDRE1)));
   UDR1 = data;
   
}
 
void USART_putstring(char* StringPtr){
 
  while(*StringPtr != 0x00){
     USART_send(*StringPtr);
     StringPtr++;}
 
}

