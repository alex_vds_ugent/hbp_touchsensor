/*Experiment Parameters*/
unsigned char maxVal = 255; //closure of finger [0,255], 255 is max closure
int duration_close = 2000; //duration (in seconds) of closing movement 
int duration_open = 2000; //duration (in seconds) of opening movement 
int repeats= 10 ; 
char WAITFORSTART =1; //wait for start signal from host pc, 0 = False, 1 = True
/*End Experiment Paramters*/

int analogPin = 6;  
int count = 0;
int N_ser = 0;

void setup() {
  //Serial.begin(9600);
  Serial.begin(256000,SERIAL_8N1); 
  delay(1000);
  Serial.flush();
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
}

void loop() {

  int delay_in_ms = round(float(duration_close)/maxVal);
  int N_open = duration_open/delay_in_ms;
  

  if (WAITFORSTART==1){ //start movement on each start signal
    while (1){

      int avail=Serial.available();
      
      if (avail>0) {
        int incomingByte = Serial.read();
        if (incomingByte==6){
          delay(2000);
          start_control(delay_in_ms,N_open); //move finger
        }
      }
      //wait for start signal
      read_and_write_pressure();
      delay(delay_in_ms);
    }
  }

  if (WAITFORSTART==0 & count==0){
    start_control(delay_in_ms,N_open); //move finger
    count++;
  }
}

void start_control(int delay_in_ms, int N_open){
    for (int r=0;r<repeats;r++){
    //close finger
    for (int i=0;i<=maxVal;i++){
      analogWrite(analogPin, i);
      delay(delay_in_ms);
      read_and_write_pressure();
    }

    //open finger
    analogWrite(analogPin, 0);
    for (int i=0;i<=N_open;i++){//should take 'duration_open' ms, keep sending pressure data
      delay(delay_in_ms);
      read_and_write_pressure();
    }
  }
}

void read_and_write_pressure(){
  //Serial.println(analogRead(A0));
  int pressure=analogRead(A0);  
  
  /*if (pressure==3){//3 is used as flag
    pressure=4;
  }*/
  Serial.write(66); // 'flag'
  Serial.write(pressure>>8); // HB
  Serial.write(pressure); // LB
}

  
  
  
