//implementing usart slave

const int I2C_ADDR = 33;
const int COLUMN_PINS[] =  {1, 0, 2, 3, 4};
const int ROW_PINS[] = {9, 10, 11, 13, 14};//, 15, 0, 1, 2, 3};

const int LEN_COLUMNS  = sizeof(COLUMN_PINS) / sizeof(int);
const int LEN_ROWS = sizeof(ROW_PINS) / sizeof(int);

int SensorRowToQuery = 0;
volatile byte SensorValues[LEN_ROWS][LEN_COLUMNS];

unsigned int BAUD_PRESCALLER = 0; //0 for 1MHz
unsigned char six = 6;

unsigned long int time0;
unsigned long int time1;
unsigned long int time2;
unsigned long int time3;
unsigned long int time4;
unsigned long int time5;
unsigned long int time6;
unsigned long int timeX;
unsigned long int timeY;

unsigned int rowsInput = 0;
unsigned int rowOutput = 0;
unsigned int readColumn = 0;

void setup() {

  /* USART init */
  /* Set baud rate */
  UBRR1H = (unsigned char)(BAUD_PRESCALLER>>8);
  UBRR1L = (unsigned char)BAUD_PRESCALLER;
  /* Enable receiver and transmitter */
  UCSR1B = (1<<RXEN1)|(1<<TXEN1);
  /* Set frame format: 8data, 2stop bit */
  UCSR1C = (1<<USBS1)|(3<<UCSZ10);
  /*set as synchronous USART*/
  UCSR1C |= (1<<UMSEL10);
  UCSR1C &= ~(1<<UMSEL11);
  

  /*set XCK1 pin as output (Master mode, internal clock used) */
  DDRC |= (1<<PC1);
  
  // init variabelen matrixen
  for (int i = 0; i < LEN_COLUMNS; i++) {
    for (int j = 0; j < LEN_ROWS; j++) {
      SensorValues[j][i] = 1;
    }
  }

  initADC();

  timeY = millis();
  time6 = micros();
}


void loop() {


  timeX = millis()-timeY;
  timeY = millis();
  time0 = micros();
  getData();
  time5 = micros();
  send_frame();
  time6 = micros();
  delay(7);
}

void initADC() {

  /* set reference */
  ADMUX &= ~(1 << REFS0); // Sets ref. voltage to VCC
  ADMUX &= ~(1 << REFS1); // Sets ref. voltage to VCC

  /* enable ADC*/
  ADCSRA |= (1 << ADEN); // Enable ADC 
    
  /* set ADC prescaler */
  ADCSRA |= (1<<ADPS0);
  ADCSRA &= ~(1<<ADPS1);
  ADCSRA &= ~(1<<ADPS2);

  /*set as left adjusted, then we can read out only ADCH if only 8bit precision needed*/
  ADCSRB |= (1<<ADLAR);
  
}

byte read_ADC(uint8_t pin) {
  //pin &=127; //strip off the high bit of the A# constants
  ADMUX = (ADMUX & 0xF0) | pin; //select the channel
  
  ADCSRA |= (1 << ADSC);         // start ADC measurement
  while (ADCSRA & (1 << ADSC) ); // wait till conversion complete

  //uint8_t low = 0;//ADCL;
  //return low;
  uint8_t high = ADCH;
  return high;
  //return (high << 8) | low;
  
}

void send_frame(){

  byte scanDurD100 = (time5-time0)/100;
  //byte rowsInput = time2-time1;
  //byte rowOutput = time3-time2;
  //byte readColumn = time4-time3;

  byte rowsInputD100 =  rowsInput/100;
  byte rowOutputD100 = rowOutput/100;
  byte readColumnD100 = readColumn/100;
  //byte ADCReg = ADCSRA;
  byte F_CPUvar = F_CPU/10000;
  
  byte timeStamps[] = {scanDurD100,rowsInputD100,rowOutputD100,readColumnD100,F_CPUvar};
  //byte timeStamps[] = {scanDur,rowsInput,rowOutput,readColumn,0};
  //byte timeStamps[] = {6, 66, 7, 77, 8};
  //byte timeStamps[] = {6,6,6,6,6};

  byte loopDur_ms = timeX;
  byte sendDurD100 = (time6-time5)/100;
  byte timeStamps2[] = {loopDur_ms,sendDurD100,66,66,66};

  for (int k = 0; k < LEN_COLUMNS; k++) { //iter columns
      byte b = timeStamps[k];
      USART_Transmit(b);
    }

  for (int k = 0; k < LEN_COLUMNS; k++) { //iter columns
      byte b = timeStamps2[k];
      USART_Transmit(b);
    }
    
  for (int j = 2; j < LEN_ROWS; j++) { //iter rows
    for (int k = 0; k < LEN_COLUMNS; k++) { //iter columns
      byte b = SensorValues[j][k];
      USART_Transmit(b);
    }
  }
}

void USART_Transmit( unsigned char data )
{
  /* Wait for empty transmit buffer */
  while ( !( UCSR1A & (1<<UDRE1)) )
  ;
  /* Put data into buffer, sends the data */
  UDR1 = data;
}


unsigned char USART_Receive( void )
{
  /* Wait for data to be received */
  while ( !(UCSR1A & (1<<RXC1)) )
  ;
  /* Get and return received data from buffer */
  return UDR1;
}


void getData() {

  rowsInput = 0;
  rowOutput = 0;
  readColumn = 0;

  //all rows high impedance
  for (int k = 0; k < LEN_ROWS; k++) {
    pinMode(ROW_PINS[k], INPUT);   // set digital pin as input
  }

  
  for (int i = 0; i < LEN_ROWS; i++) {

    time1 = micros();
    // turn on selected row
    pinMode(ROW_PINS[i], OUTPUT);   // set digital pin as output
    digitalWrite(ROW_PINS[i], HIGH);
    time3 = micros();

    int diff2 = time3-time1;
    rowOutput += diff2;

    // read out analog inputs
    for (int j = 0; j < LEN_COLUMNS; j++) { 
      //SensorValues[i][j] = analogRead(COLUMN_PINS[j]);

      SensorValues[i][j] = read_ADC(COLUMN_PINS[j]);
      
    }
    time4 = micros();

    int diff3 = time4-time3;
    readColumn += diff3;

    
    pinMode(ROW_PINS[i], INPUT);   // set digital pin back as input
    time2 = micros();

    int diff=time2-time4;
    rowsInput+=diff;

    
  }
}


