//implementing usart slave

const int I2C_ADDR = 33;
const int COLUMN_PINS[] =  {1, 0, 2, 3, 4};
const int ROW_PINS[] = {9, 10, 11, 13, 14};//, 15, 0, 1, 2, 3};

const int LEN_COLUMNS  = sizeof(COLUMN_PINS) / sizeof(int);
const int LEN_ROWS = sizeof(ROW_PINS) / sizeof(int);

int SensorRowToQuery = 0;
volatile int SensorValues[LEN_ROWS][LEN_COLUMNS];

unsigned int BAUD_PRESCALLER = 0; //0 for 1MHz
unsigned char six = 6;

unsigned long int time0;
unsigned long int time1;
unsigned long int time2;
unsigned long int time3;
unsigned long int time4;
unsigned long int time5;

unsigned int rowsInput = 0;
unsigned int rowOutput = 0;
unsigned int readColumn = 0;

void setup() {

  /* USART init */
  /* Set baud rate */
  UBRR1H = (unsigned char)(BAUD_PRESCALLER>>8);
  UBRR1L = (unsigned char)BAUD_PRESCALLER;
  /* Enable receiver and transmitter */
  UCSR1B = (1<<RXEN1)|(1<<TXEN1);
  /* Set frame format: 8data, 2stop bit */
  UCSR1C = (1<<USBS1)|(3<<UCSZ10);
  /*set as synchronous USART*/
  UCSR1C |= (1<<UMSEL10);
  UCSR1C &= ~(1<<UMSEL11);
  

  /*set XCK1 pin as output (Master mode, internal clock used) */
  DDRC |= (1<<PC1);
  
  // init variabelen matrixen
  for (int i = 0; i < LEN_COLUMNS; i++) {
    for (int j = 0; j < LEN_ROWS; j++) {
      SensorValues[j][i] = 1;
    }
  }
}

void loop() {

  time0 = millis();
  getData();
  time5 = millis();
  
  send_frame();

  delay(100);
}


void send_frame(){

  byte scanDur = time5-time0;
  //byte rowsInput = time2-time1;
  //byte rowOutput = time3-time2;
  //byte readColumn = time4-time3;

  byte rowsInputD100 =  rowsInput/100;
  byte rowOutputD100 = rowOutput/100;
  byte readColumnD100 = readColumn/100;

  byte timeStamps[] = {scanDur,rowsInputD100,rowOutputD100,readColumnD100,0};
  //byte timeStamps[] = {scanDur,rowsInput,rowOutput,readColumn,0};
  //byte timeStamps[] = {6, 66, 7, 77, 8};
  //byte timeStamps[] = {6,6,6,6,6};

  for (int k = 0; k < LEN_COLUMNS; k++) { //iter columns
      byte b = timeStamps[k];
      USART_Transmit(b);
    }
    
  for (int j = 1; j < LEN_ROWS; j++) { //iter rows
    for (int k = 0; k < LEN_COLUMNS; k++) { //iter columns
      byte b = SensorValues[j][k] / 4;
      USART_Transmit(b);
    }
  }
}

void USART_Transmit( unsigned char data )
{
  /* Wait for empty transmit buffer */
  while ( !( UCSR1A & (1<<UDRE1)) )
  ;
  /* Put data into buffer, sends the data */
  UDR1 = data;
}


unsigned char USART_Receive( void )
{
  /* Wait for data to be received */
  while ( !(UCSR1A & (1<<RXC1)) )
  ;
  /* Get and return received data from buffer */
  return UDR1;
}


void getData() {

  rowsInput = 0;
  rowOutput = 0;
  readColumn = 0;

  //all rows high impedance
  for (int k = 0; k < LEN_ROWS; k++) {
    pinMode(ROW_PINS[k], INPUT);   // set digital pin as input
  }

  
  for (int i = 0; i < LEN_ROWS; i++) {

    time1 = micros();
    // turn on selected row
    pinMode(ROW_PINS[i], OUTPUT);   // set digital pin as output
    digitalWrite(ROW_PINS[i], HIGH);
    time3 = micros();

    int diff2 = time3-time1;
    rowOutput += diff2;

    // read out analog inputs
    for (int j = 0; j < LEN_COLUMNS; j++) { 
      SensorValues[i][j] = analogRead(COLUMN_PINS[j]);
    }
    time4 = micros();

    int diff3 = time4-time3;
    readColumn += diff3;

    
    pinMode(ROW_PINS[i], INPUT);   // set digital pin back as input
    time2 = micros();

    int diff=time2-time4;
    rowsInput+=diff;

    
  }
}


