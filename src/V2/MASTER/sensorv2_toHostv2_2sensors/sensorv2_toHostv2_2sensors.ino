
int rows1[] = {A0,A1,A2};//{A1,A2,A0};
int cols1[] = {13,12,11};//{11,10,9,8,12}; 
int rows2[] = {A3,A4,A5};//{A1,A2,A0};
int cols2[] = {10,9,8};//{11,10,9,8,12}; 

const int LEN_ROWS = 3;
const int LEN_COLUMNS = 6;

volatile int SensorValues[LEN_ROWS][LEN_COLUMNS];
const int SENSORID = 27;
const int N_INTERFLAGFRAMES = 100; // N frames between flag, flag used to synchronize Serial with pc
const int RESOLUTION = 10;
volatile uint8_t FlagCount =0;


void setup() {

  // init serial
  Serial.begin(256000,SERIAL_8N1);
  //Serial.begin(9600);
  delay(1000);
  Serial.flush();

  void init_IO();
} 

void loop() {

  read_frame();
  //print_frame(); //requires Serial.begin(9600);
  write_flag(SENSORID);
  write_frame();

}

void read_frame(){
  
    for (int colCount = 0; colCount < 3; colCount++) { 
      pinMode(cols1[colCount], OUTPUT); // set as OUTPUT 
      digitalWrite(cols1[colCount], HIGH); // set high
      for (int rowCount = 0; rowCount < 3; rowCount++) {
        int dummy = analogRead(rows1[rowCount]); // read INPUT 
        SensorValues[rowCount][colCount] = analogRead(rows1[rowCount]); // read INPUT again, possibly multiplex timing probem
      }// end rowCount 
      pinMode(cols1[colCount], INPUT); // set back to INPUT! 
  }// end colCount 
  

      for (int colCount = 0; colCount < 3; colCount++) { 
      pinMode(cols2[colCount], OUTPUT); // set as OUTPUT 
      digitalWrite(cols2[colCount], HIGH); // set high
        for (int rowCount = 0; rowCount < 3; rowCount++) {
          int dummy = analogRead(rows2[rowCount]); // read INPUT 
          SensorValues[rowCount][3+colCount] = analogRead(rows2[rowCount]); // read INPUT again, possibly multiplex timing probem
        }// end rowCount 
      pinMode(cols2[colCount], INPUT); // set back to INPUT! 
  }// end colCount 


}

void print_frame() {

  for (int j = 0; j < LEN_ROWS; j++) {
    for (int k = 0; k < LEN_COLUMNS; k++) {
      Serial.print(SensorValues[j][k]);
      Serial.print(",");
    }
    Serial.println("");
  }

  Serial.println("");
}

void write_flag(uint8_t sensorID){
  //Flag is used to synchronize serial communication and to transmit info
  FlagCount+=1;
  if (FlagCount == N_INTERFLAGFRAMES){
    Serial.write(99);
    Serial.write(33);
    Serial.write(66);
    Serial.write(N_INTERFLAGFRAMES);
    Serial.write(sensorID);
    Serial.write(LEN_ROWS);
    Serial.write(LEN_COLUMNS);
    Serial.write(RESOLUTION);
    FlagCount=0;
  }
}

void write_frame() {
  //write matrix to host
  for (int j = 0; j < LEN_ROWS; j++) {
    for (int k = 0; k < LEN_COLUMNS; k++) {
      Serial.write(SensorValues[j][k]);
    }
  }

  if (RESOLUTION==10){ //send second frame with HBs
    //write matrix to host
    for (int j = 0; j < LEN_ROWS; j++) {
      for (int k = 0; k < LEN_COLUMNS; k++) {
        Serial.write(SensorValues[j][k]>>8);
      }
    }
  }     

  
}

void init_IO() {
  // set all rows and columns to INPUT (high impedance):
  for (int i = 0; i < 3; i++) { 
    pinMode(rows1[i],INPUT);
    pinMode(rows2[i],INPUT);
  } 
  for (int i = 0; i < 3; i++) { 
    pinMode(cols1[i], INPUT); 
    pinMode(cols2[i], INPUT); 
  } 
}

