import numpy as np
import os
import sys
import time
import datetime
import natsort
import tables
from PyQt5 import QtCore, QtWidgets, QtGui
import qdarkstyle

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import cm  # Color maps
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
import matplotlib.animation as animation
from matplotlib.lines import Line2D
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.patches import Circle
import matplotlib.gridspec as gridspec

#print sys.path
sys.path+=["/home/alexander/Documents/Spikingsensor/Code/src/PROCESS"]
#print sys.path

import TouchSensor
import threading
import psutil
import serial


"""
TODO
    [x] refresh experiment list after recording
    [x] input recording duration
    [x] recording feedback in screen
    [x] print timestamp during replay
    [x] experiment naam
    [x] fix newline printing
    [x] catch 'no uC found'/'device busy'
    [] input box text outside of box000
    [] live feed during recording
    [] save dir0
    [x] try without serial.readline for speed?
    [] inputs:
        [] sensor dimensions
        [] checkboxes:
            [] autoscale during live feed
            [] DEBUG (verbosity)
    
"""


class AppWin(QtWidgets.QMainWindow):
    def __init__(self, folder):
        # Init

        QtWidgets.QMainWindow.__init__(self)

        self.folder = folder
        self.sel_exp = None
        self.sel_ite = None
        self.sel_conf = None
        self.last_action = None

        self.initUI()

    def initUI(self):
        self.resize(1600, 1200)

        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("Touch Sensor Data Viewer")
        self.displayStatus("chirp chirp", 4000)
        self.setWindowIcon(self.style().standardIcon(getattr(QtWidgets.QStyle, 'SP_ComputerIcon')))

        self.constructUI()
        self.moveUI()
        self.show()

    def constructUI(self):
        # Create top menu and shortcuts
        # self.file_menu = QtWidgets.QMenu('&File', self)
        # self.file_menu.addAction('&Quit', self.quitUI, QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        # self.menuBar().addMenu(self.file_menu)
        QtWidgets.QShortcut(QtGui.QKeySequence("Ctrl+C"), self, self.quitUI)
        QtWidgets.QShortcut(QtGui.QKeySequence("Ctrl+D"), self, self.quitUI)
        QtWidgets.QShortcut(QtGui.QKeySequence("Ctrl+W"), self, self.quitUI)

        # Create frame structure
        self.main_window = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        #win_width = self.frameGeometry().width()
        #win_height = self.frameGeometry().height()

        ## Create two panels, 1 for selection and 1 for visualization
        self.main_sel_pan = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.main_vis_pan = QtWidgets.QSplitter(QtCore.Qt.Vertical)


        ### Create a selection list and add to selection panel
        self.exp_list = QtWidgets.QWidget()
        self.exp_list_lay = ExpListWin(self)
        self.exp_list.setLayout(self.exp_list_lay)
        self.main_sel_pan.addWidget(self.exp_list) # add to panel

        ### Create another selection list and add to selection panel
        #self.ite_list_lay = IteListWin(self)
        # self.ite_list = QtWidgets.QWidget()
        # self.ite_list.setLayout(self.ite_list_lay)
        # self.main_sel_pan.addWidget(self.ite_list)


        ### Create visualisation buttons and add to visualisation panel
        self.viz_butt = QtWidgets.QWidget()
        self.viz_butt_lay = VizButtonsWin(self)
        self.viz_butt.setLayout(self.viz_butt_lay)
        self.main_vis_pan.addWidget(self.viz_butt) # add to panel

        ### Create experiment buttons and add to visualisation panel
        self.exp_butt = QtWidgets.QWidget()
        self.exp_butt_lay = ExpButtonsWin(self)
        self.exp_butt.setLayout(self.exp_butt_lay)
        self.main_vis_pan.addWidget(self.exp_butt) # add to panel

        ### Create visualisation frame and add to visualisation panel
        self.viz = QtWidgets.QWidget()
        self.viz_lay = VizWin(self)
        self.viz.setLayout(self.viz_lay)
        self.main_vis_pan.addWidget(self.viz)



        ### Create more buttons and add to visualisation panel
        # self.ite_butt_lay = IteButWin(self)
        # self.ite_butt = QtWidgets.QWidget()
        # self.ite_butt.setLayout(self.ite_butt_lay)
        # self.main_vis_pan.addWidget(self.ite_butt)

        ## add panels to main window
        self.main_window.addWidget(self.main_sel_pan)
        self.main_window.addWidget(self.main_vis_pan)

        # Set focus
        self.main_window.setFocus()
        self.setCentralWidget(self.main_window)

    def moveUI(self):
        frameGm = self.frameGeometry()
        screen = QtWidgets.QApplication.desktop().screenNumber(QtWidgets.QApplication.desktop().cursor().pos())
        centerPoint = QtWidgets.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())

    def resizeEvent(self, event):
        win_width = self.frameGeometry().width()
        win_height = self.frameGeometry().height()

        self.exp_list.setMinimumWidth(win_width / 5)
        self.exp_list.setMaximumWidth(win_width / 3)
        #self.viz_butt.setMinimumHeight(win_height / 10)
        #self.viz_butt.setMaximumHeight(win_height / 7)

        #self.viz.setMinimumHeight(win_height / 2)
        #self.viz.setMaximumHeight(9 * win_height / 10)

        QtWidgets.QMainWindow.resizeEvent(self, event)

    def cleanup(self):
        print('\n\n -- Quitting and killing all children processes! -- \n')
        process = psutil.Process()
        children = process.children(recursive=True)
        time.sleep(0.2)
        for p in children:
            p.kill()
        process.kill()

    def quitUI(self):
        self.close()

    def closeEvent(self, ce):
        self.cleanup()
        self.quitUI()

    def displayStatus(self, msg, t=1000):
        self.statusBar().showMessage(msg, t)


class ExpListWin(QtWidgets.QGridLayout):

    def __init__(self, win):

        QtWidgets.QGridLayout.__init__(self)
        self.setGeometry(QtCore.QRect(0, 0, 150, 400))

        self.label = QtWidgets.QLabel()
        self.label.setText("Experiment List")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.addWidget(self.label)

        self.list = QtWidgets.QListWidget()
        self.list.setMinimumWidth(200)
        self.list.setMaximumWidth(600)
        self.addWidget(self.list)

        self.win = win
        self.loadExperiments()
        self.list.itemClicked.connect(self.selectExperiment)
        self.list.currentItemChanged.connect(self.selectExperiment)

        self.b1 = QtWidgets.QPushButton("Refresh")
        self.b1.installEventFilter(self)
        self.addWidget(self.b1, 2, 0)

    def loadExperiments(self):
        self.list.clear()
        for dirname, dirnames, filenames in os.walk(self.win.folder):
            filenames = natsort.natsorted(filenames,reverse=True)

            for filename in filenames:
                #print subdirname
                #date = subdirname.split('_')[0]
                #date = datetime.datetime.strptime(date, '%Y-%m-%d-%H-%M-%S')
                item = QtWidgets.QListWidgetItem()
                #text = date.strftime("Exp %d/%m/%Y - %H:%M:%S") + ' - ' +
                item.setText(filename)
                item.setData(QtCore.Qt.UserRole, dirname + "/" + filename)
                self.list.addItem(item)


    def selectExperiment(self, item):
        if item:
            self.win.sel_exp = item.data(QtCore.Qt.UserRole)
        #self.win.ite_list_lay.loadIterations()
        return

    def eventFilter(self, object, event):

        if event.type() == QtCore.QEvent.MouseButtonPress:
            if object.text() == "Refresh":
                print 'refreshing'
                self.loadExperiments()
                return True

        elif event.type() == QtCore.QEvent.HoverMove:
            return self.displayHelp(object.text())

        return False

    def displayHelp(self, action):

        if action == "Simulate Best":
            self.win.displayStatus("Replay the simulation of the best individual in the experiment")
        elif action == "Optim Parameters":
            self.win.displayStatus("Display the parameters of the optimization process")
        elif action == "CMA Evolution":
            self.win.displayStatus("Display the optimization evolution across generations")
        elif action == "Parameters Evolution":
            self.win.displayStatus("Display the parameters convergence across generations")
        return True

class VizButtonsWin(QtWidgets.QGridLayout):
    def __init__(self, win):

        QtWidgets.QGridLayout.__init__(self)
        self.setGeometry(QtCore.QRect(0, 0, 1000, 100))

        self.win = win

        self.addLegend()
        self.addButtonsAndBoxes()


    def addButtonsAndBoxes(self):
        """
        self.b1 = QtWidgets.QPushButton("3D replay")
        self.b1.installEventFilter(self)
        self.addWidget(self.b1, 1, 0,1,2)

        self.b3 = QtWidgets.QPushButton("Live feed")
        self.b3.installEventFilter(self)
        self.addWidget(self.b3, 2, 0,1,1)

        self.b2 = QtWidgets.QPushButton("Spiking replay")
        self.b2.installEventFilter(self)
        self.addWidget(self.b2, 1, 3,1,1)

        self.b4 = QtWidgets.QPushButton("Spiking replay Reichardt")
        self.b4.installEventFilter(self)
        self.addWidget(self.b4, 2, 2,1,2)


        """
        self.b1 = QtWidgets.QPushButton("3D replay")
        self.b1.installEventFilter(self)
        self.addWidget(self.b1, 1, 0,1,2)

        self.b3 = QtWidgets.QPushButton("Live feed")
        self.b3.installEventFilter(self)
        self.addWidget(self.b3, 2, 0,1,2)

        self.b5 = QtWidgets.QPushButton("Live feed_spiking")
        self.b5.installEventFilter(self)
        self.addWidget(self.b5, 3, 0,1,2)

        self.b7 = QtWidgets.QPushButton("Mean activity")
        self.b7.installEventFilter(self)
        self.addWidget(self.b7, 4, 0,1,2)

        self.b9 = QtWidgets.QPushButton("Mean activity + spikes")
        self.b9.installEventFilter(self)
        self.addWidget(self.b9, 5, 0,1,2)

        self.b11 = QtWidgets.QPushButton("Distance measurements")
        self.b11.installEventFilter(self)
        self.addWidget(self.b11, 6, 0,1,2)

        self.replay_fps = QtWidgets.QLineEdit("replay fps")
        self.addWidget(self.replay_fps, 7, 0,1,2)

        self.b2 = QtWidgets.QPushButton("Spiking replay")
        self.b2.installEventFilter(self)
        self.addWidget(self.b2, 1, 2,1,2)

        self.b4 = QtWidgets.QPushButton("Spiking replay Reichardt")
        self.b4.installEventFilter(self)
        self.addWidget(self.b4, 2, 2,1,2)

        self.b6 = QtWidgets.QPushButton("Spiking replay double edge")
        self.b6.installEventFilter(self)
        self.addWidget(self.b6, 3, 2,1,2)

        self.b8 = QtWidgets.QPushButton("Mean column activity")
        self.b8.installEventFilter(self)
        self.addWidget(self.b8, 4, 2,1,2)

        self.b10 = QtWidgets.QPushButton("Mean activity + spikes + histogram")
        self.b10.installEventFilter(self)
        self.addWidget(self.b10, 5, 2,1,2)

        self.b12 = QtWidgets.QPushButton("All traces")
        self.b12.installEventFilter(self)
        self.addWidget(self.b12, 6, 2,1,1)

        # self.b12b = QtWidgets.QPushButton("chckbx")
        # self.b12b.installEventFilter(self)
        # self.addWidget(self.b12b, 6, 3,1,1)

        self.b12b = QtWidgets.QCheckBox("InverseRowCol")
        #self.b2.toggled.connect(self.)
        self.addWidget(self.b12b, 6, 3, 1, 1)

        self.spike_threshold = QtWidgets.QLineEdit("spike threshold")
        self.addWidget(self.spike_threshold, 7, 2,1,2)

        self.frames_to_skip = QtWidgets.QLineEdit("frames_to_skip")
        self.addWidget(self.frames_to_skip, 8, 2,1,2)

        return

    def addInputBoxes(self):

        # self.replay_fps = QtWidgets.QLineEdit("replay fps")
        # self.addWidget(self.replay_fps, 5, 0)
        # self.spike_threshold = QtWidgets.QLineEdit("spike threshold")
        # self.addWidget(self.spike_threshold, 5, 1)
        return

    def addLegend(self):

        self.l1 = QtWidgets.QLabel()
        self.l1.setText("Visualization Functions")
        self.l1.setAlignment(QtCore.Qt.AlignCenter)
        self.addWidget(self.l1, 0, 0, 1, 2)

    def eventFilter(self, object, event):

        #print 'im in filter?'
        if event.type() == QtCore.QEvent.MouseButtonPress:
            print 'CLICKED'
            return self.dispatchAction(object.text())

        elif event.type() == QtCore.QEvent.HoverMove:
            #print 'hovereventtt'
            return self.displayHelp(object.text())

        return False

    def dispatchAction(self, action):

        if self.win.sel_exp:
            print self.win.sel_exp
            self.win.viz_lay.plotFigure(action)
            self.win.last_action = action
            return True
        else:
            print 'selection experiment'
            self.win.displayStatus("Please select experiment before using this function", 3000)
            return True

    def displayHelp(self, action):

        if action == "Simulate Best":
            self.win.displayStatus("Replay the simulation of the best individual in the experiment")
        elif action == "Optim Parameters":
            self.win.displayStatus("Display the parameters of the optimization process")
        elif action == "CMA Evolution":
            self.win.displayStatus("Display the optimization evolution across generations")
        elif action == "Parameters Evolution":
            self.win.displayStatus("Display the parameters convergence across generations")
        return True

class ExpButtonsWin(QtWidgets.QGridLayout):
    def __init__(self, win):

        QtWidgets.QGridLayout.__init__(self)
        self.setGeometry(QtCore.QRect(0, 0, 1000, 100))

        self.win = win

        self.addLegend()
        self.addButtons()
        self.addInputBoxes()


    def addLegend(self):

        self.l1 = QtWidgets.QLabel()
        self.l1.setText("Experiment Functions")
        self.l1.setAlignment(QtCore.Qt.AlignCenter)
        self.addWidget(self.l1, 0, 0, 1, 2)
        return

    def addButtons(self):

        self.butt1 = QtWidgets.QPushButton("Start Recording")
        self.butt1.installEventFilter(self)
        self.addWidget(self.butt1, 3, 0)

        self.butt2 = QtWidgets.QPushButton("Stop Recording")
        self.butt2.installEventFilter(self)
        self.addWidget(self.butt2, 4, 0)
        return

    def addInputBoxes(self):

        self.rec_dur = QtWidgets.QLineEdit("rec time (s)")
        #self.rec_dur.move(80, 20)
        #self.rec_dur.resize(20, 32)
        self.addWidget(self.rec_dur, 1, 0)

        self.exp_name = QtWidgets.QLineEdit("exp_name")
        #self.exp_name.move(60, 20)
        #self.exp_name.resize(10, 10)
        self.addWidget(self.exp_name, 2, 0)
        return

    def eventFilter(self, object, event):

        #print 'im in filter?'
        if event.type() == QtCore.QEvent.MouseButtonPress:
            print 'CLICKED'
            return self.dispatchAction(object.text())

        elif event.type() == QtCore.QEvent.HoverMove:
            #print 'hovereventtt'
            return self.displayHelp(object.text())

        return False

    def dispatchAction(self, action):

        # if self.win.sel_exp:
        #     print self.win.sel_exp
        #     self.win.viz_lay.plotFigure(action)
        #     self.win.last_action = action
        #     return True
        # else:
        #     print 'selection administration'
        #     self.win.displayStatus("Please select experiment before using this function", 3000)
        #     return True

        self.win.viz_lay.performExperiment(action)
        self.win.last_action = action
        return True

    def displayHelp(self, action):

        if action == "Simulate Best":
            self.win.displayStatus("Replay the simulation of the best individual in the experiment")
        elif action == "Optim Parameters":
            self.win.displayStatus("Display the parameters of the optimization process")
        elif action == "CMA Evolution":
            self.win.displayStatus("Display the optimization evolution across generations")
        elif action == "Parameters Evolution":
            self.win.displayStatus("Display the parameters convergence across generations")
        return True

class VizWin(QtWidgets.QGridLayout):
    def __init__(self, win):

        QtWidgets.QGridLayout.__init__(self)

        self.setGeometry(QtCore.QRect(0, 0, 1000, 600))

        self.win = win

        self.list = QtWidgets.QListWidget()
        self.list.setMinimumWidth(50)
        self.list.setMaximumWidth(500)
        self.list.setMaximumHeight(100)
        self.addWidget(self.list)

        self.item = QtWidgets.QListWidgetItem()
        # text = date.strftime("Exp %d/%m/%Y - %H:%M:%S") + ' - ' +
        self.item.setText('output...')
        #item.setData(QtCore.Qt.UserRole, dirname + "/" + filename)
        self.list.addItem(self.item)


    def plotFigure(self, name):

        if name == "3D replay":
            self.replay_3D()
        if name == "Spiking replay":
            self.spiking_replay()
        if name == "Spiking replay Reichardt":
            self.spiking_replay_motion('Reichardt')
        if name == "Spiking replay double edge":
            self.spiking_replay_motion('doubleEdge')
        if name == "Live feed":
            self.live_feed()
        if name == "Live feed_spiking":
            self.spiking_motion_live()
        if name == "Mean activity":
            self.plot_mean_activity()
        if name == "Mean column activity":
            self.plot_mean_column_activity()
        if name == "Mean activity + spikes":
            self.plot_mean_activity_spikes()
        if name == "Mean activity + spikes + histogram":
            self.plot_mean_activity_spikes(histogram=True)
        if name == "Distance measurements":
            self.plot_distance_and_servo()
        if name == "All traces":
            self.plot_all_traces()
        return


    def replay_3D(self):

        #read input boxes
        try:
            replay_fps = int(self.win.viz_butt_lay.replay_fps.text())
        except:
            replay_fps = 50

        #reload(readPlotSavedata)
        dataFile = self.win.sel_exp#'/home/alexander/Documents/ExperimentData/FlexSensor/2018-12-17-17-14-15_test_flexSensor_dataSave'
        print self.win.sel_exp

        Vis = TouchSensor.Visualiser(dataFile)
        #time.sleep(3)
        Vis.animate_3D(fps=replay_fps)
        #try:
        #    Vis.animate()
        #except:
        #    print 'try again?'
        #ani = 0
        plt.show()


    def spiking_replay(self):

        #read input boxes
        try:
            replay_fps = int(self.win.viz_butt_lay.replay_fps.text())
        except:
            replay_fps = 20
        try:
            self.spike_threshold = int(self.win.viz_butt_lay.spike_threshold.text())
        except:
            self.spike_threshold = 1

        # get data
        dataFile = self.win.sel_exp  # '/home/alexander/Documents/ExperimentData/FlexSensor/2018-12-17-17-14-15_test_flexSensor_dataSave'
        self.Vis = TouchSensor.Visualiser(dataFile)

        self.data = self.Vis.data
        self.data[0][0][0] = 50

        rec_dur = self.Vis.timestamps[-1]
        recording_FR = self.data.shape[0]/rec_dur



        self.fig = plt.figure()

        ax_analog = self.fig.add_subplot(211)  # , projection='3d')
        ax_analog.set_title('analog mode')
        self.im_an = ax_analog.imshow(self.data[0], cmap=cm.jet, interpolation='none', animated=True)
        cbar_analog = self.fig.colorbar(self.im_an)
        ax_analog.add_artist(self.im_an)

        self.ax_spiking = self.fig.add_subplot(212)  # create an axis
        self.ax_spiking.set_title('diff spiking mode')
        #self.ax_spiking.hold(False)  # discards the old graph
        spike_matrix_init = np.zeros((self.Vis.NROWS,self.Vis.NCOLS)).astype('uint8')
        spike_matrix_init[0][0]=2
        self.im_sp = self.ax_spiking.imshow(spike_matrix_init, cmap='gray', interpolation='none', animated=True)
        cbar_spiking = self.fig.colorbar(self.im_sp,ticks=[0,1,2])
        cbar_spiking.ax.set_yticklabels(['-', '0', '+'])
        self.ax_spiking.add_artist(self.im_sp)

        plt.suptitle(
            'Recording: '+str(self.Vis.N_frames) + ' frames in ' + str(rec_dur) + 's (' + str(recording_FR) + 'Hz)'
            + 'fps, spike_threshold: ' + str(self.spike_threshold))

        #self.ax.set_xlim([0,10])
        #self.ax.set_ylim([-2,2])
        interval = 1000/replay_fps # in ms
        self.anim = animation.FuncAnimation(self.fig,self.update_spiking_replay,frames=self.Vis.N_frames,interval=interval,repeat=False,blit=True)
        plt.show()

    def update_spiking_replay(self, i):

        if i < self.Vis.N_frames:
            self.im_an.set_array(self.data[i])

            spike_matrix = TouchSensor.analog2diff(self.data[i],self.data[i-1],self.spike_threshold) #self.make_spike_matrix(i)
            # print self.data[i]
            # print spike_matrix
            # print type(self.data[i][0][0])
            # print type(spike_matrix[0][0])
            print 'frame '+str(i)+'/'+str(self.Vis.N_frames)
            print spike_matrix

            self.im_sp.set_array(spike_matrix)
        else:
            #self.ax_spike.set_xlabel('frame ' + str(self.N_frames) + '/' + str(self.N_frames))
            pass

        return self.im_an,self.im_sp,



    # def spiking_replay_motion(self):
    #
    #     # read input boxes
    #     try:
    #         replay_fps = int(self.win.viz_butt_lay.replay_fps.text())
    #     except:
    #         replay_fps = 20
    #     try:
    #         self.spike_threshold = int(self.win.viz_butt_lay.spike_threshold.text())
    #     except:
    #         self.spike_threshold = 1
    #
    #         # get data
    #     dataFile = self.win.sel_exp  # '/home/alexander/Documents/ExperimentData/FlexSensor/2018-12-17-17-14-15_test_flexSensor_dataSave'
    #     self.Vis = TouchSensor.Visualiser(dataFile)
    #
    #     self.data = self.Vis.data  # np.array(self.Vis.data).reshape(-1, 5, 5)  # .astype(numpy.float32)
    #     self.data[0][0][0] = 50
    #
    #     rec_dur = self.Vis.timestamps[-1]
    #     recording_FR = self.data.shape[0]/rec_dur
    #
    #
    #     self.fig = plt.figure(figsize=(10,10))
    #
    #     #analog axis
    #     ax_analog = self.fig.add_subplot(311)  # , projection='3d')
    #     ax_analog.set_title('analog mode')
    #     self.im_an = ax_analog.imshow(self.data[0], cmap=cm.jet, interpolation='none', animated=True)
    #     ax_analog.add_artist(self.im_an)
    #     cbar_analog = self.fig.colorbar(self.im_an)
    #
    #     #diff spiking axis
    #     ax_spiking = self.fig.add_subplot(312)  # create an axis
    #     ax_spiking.set_title('diff spiking mode')
    #     # self.ax_spiking.hold(False)  # discards the old graph
    #     spike_matrix_init = np.zeros((self.Vis.NCOLS, self.Vis.NCOLS)).astype('uint8')
    #     spike_matrix_init[0][0] = 2
    #     cmap = mpl.colors.ListedColormap(['black', 'gray', 'white'])
    #     self.im_sp = ax_spiking.imshow(spike_matrix_init, cmap=cmap, interpolation='none', animated=True)
    #     ax_spiking.add_artist(self.im_sp)
    #     #cbar_spiking = self.fig.colorbar(self.im_sp)
    #     cbar_spiking = self.fig.colorbar(self.im_sp,ticks=[0,1,2])
    #     cbar_spiking.ax.set_yticklabels(['-', '0', '+'])
    #
    #     #motion spiking axis
    #     ax_spiking_motion = self.fig.add_subplot(313)  # create an axis
    #     ax_spiking_motion.set_title('motion spiking mode')
    #     # self.ax_spiking.hold(False)  # discards the old graph
    #     spike_matrix_init = np.ones((self.Vis.NCOLS, self.Vis.NCOLS)).astype('uint8')
    #     spike_matrix_init[0][0] = 2
    #     self.im_sp_mo = ax_spiking_motion.imshow(spike_matrix_init, cmap='gray', interpolation='none', animated=True)
    #     ax_spiking_motion.add_artist(self.im_sp_mo)
    #     cbar_motion = self.fig.colorbar(self.im_sp_mo,ticks=[0,1,2])
    #     cbar_motion.ax.set_yticklabels(['-', '0', '+'])
    #
    #     plt.suptitle(
    #         'Recording: '+str(self.Vis.N_frames) + ' frames in ' + str(rec_dur) + 's (' + str(recording_FR) + 'Hz)'
    #         + ', playback speed: ' + str(replay_fps) + 'fps, spike_threshold: ' + str(self.spike_threshold))
    #
    #     # self.ax.set_xlim([0,10])
    #     # self.ax.set_ylim([-2,2])
    #     interval = 1000 / replay_fps  # in ms
    #     self.anim = animation.FuncAnimation(self.fig, self.update_spiking_replay_motion,frames=self.Vis.N_frames, interval=interval, repeat=False,
    #                                         blit=True)
    #     plt.show()
    #
    # def update_spiking_replay_motion(self, i):
    #
    #     if i ==0:
    #         self.prev_spike_matrix = np.ones((self.Vis.NCOLS, self.Vis.NCOLS)).astype('int8')
    #
    #     if i < self.Vis.N_frames:
    #         self.im_an.set_array(self.data[i])
    #
    #         spike_matrix = TouchSensor.analog2diff(self.data[i], self.data[i - 1],
    #                                                self.spike_threshold)  # self.make_spike_matrix(i)
    #         spike_matrix_motion = TouchSensor.diff2motion(spike_matrix,self.prev_spike_matrix)
    #
    #         print self.data[i]
    #         print spike_matrix
    #         print spike_matrix_motion
    #
    #         self.im_sp.set_array(spike_matrix)
    #         self.im_sp_mo.set_array(spike_matrix_motion)
    #     else:
    #         # self.ax_spike.set_xlabel('frame ' + str(self.N_frames) + '/' + str(self.N_frames))
    #         pass
    #
    #     self.prev_spike_matrix = spike_matrix
    #     return self.im_an, self.im_sp,self.im_sp_mo,


    def spiking_replay_motion(self, motion_type='doubleEdge'):

        self.motion_type = motion_type
        self.reichardt_delay=1

        # read input boxes
        try:
            replay_fps = int(self.win.viz_butt_lay.replay_fps.text())
        except:
            replay_fps = 20
        try:
            self.spike_threshold = int(self.win.viz_butt_lay.spike_threshold.text())
        except:
            self.spike_threshold = 1

            # get data
        dataFile = self.win.sel_exp  # '/home/alexander/Documents/ExperimentData/FlexSensor/2018-12-17-17-14-15_test_flexSensor_dataSave'
        self.Vis = TouchSensor.Visualiser(dataFile)

        self.data = self.Vis.data  # np.array(self.Vis.data).reshape(-1, 5, 5)  # .astype(numpy.float32)
        self.data[0][0][0] = 50


        rec_dur = self.Vis.timestamps[-1]
        recording_FR = self.data.shape[0]/rec_dur




        self.fig = plt.figure(figsize=(9,12))
        gs = gridspec.GridSpec(nrows=5, ncols=3, height_ratios=[2,2,2,2,2])

        #analog axis
        ax_analog = self.fig.add_subplot(gs[0,1])  # , projection='3d')
        ax_analog.set_title('analog mode')
        self.im_an = ax_analog.imshow(self.data[0], cmap=cm.jet, interpolation='none', animated=True)
        ax_analog.add_artist(self.im_an)
        cbar_analog = self.fig.colorbar(self.im_an)

        #diff spiking axis
        ax_spiking = self.fig.add_subplot(gs[1,1])  # create an axis
        ax_spiking.set_title('diff spiking mode')
        # self.ax_spiking.hold(False)  # discards the old graph
        spike_matrix_init = np.zeros((self.Vis.NCOLS, self.Vis.NCOLS)).astype('uint8')
        spike_matrix_init[0][0] = 2
        cmap = mpl.colors.ListedColormap(['black', 'gray', 'white'])
        self.im_sp = ax_spiking.imshow(spike_matrix_init, cmap=cmap, interpolation='none', animated=True)
        ax_spiking.add_artist(self.im_sp)
        #cbar_spiking = self.fig.colorbar(self.im_sp)
        cbar_spiking = self.fig.colorbar(self.im_sp,ticks=[0,1,2])
        cbar_spiking.ax.set_yticklabels(['-', '0', '+'])



        spike_matrix_init = np.zeros((self.Vis.NCOLS, self.Vis.NCOLS)).astype('uint8')
        spike_matrix_init[0][0] = 2
        cmap = mpl.colors.ListedColormap(['black','gray', 'white'])
        locs = [[2,1],[3,0],[3,2],[4,1]]
        self.motion_imshows = []

        for idx, itm in enumerate(['up','left','right','down']):
            # motion spiking axis
            ax = self.fig.add_subplot(gs[locs[idx][0], locs[idx][1]])  # create an axis
            ax.set_title(itm)
            self.motion_imshows.append(ax.imshow(spike_matrix_init, cmap=cmap, interpolation='none', animated=True))
            ax.add_artist(self.motion_imshows[-1])
            cbar_motion = self.fig.colorbar(self.motion_imshows[-1], ticks=[0, 1,2])
            cbar_motion.ax.set_yticklabels(['dead','0', '+'])
        plt.tight_layout()

        #plt.suptitle(
        #    'Recording: '+str(self.Vis.N_frames) + ' frames in ' + str(rec_dur) + 's (' + str(recording_FR) + 'Hz)'
        #    + ', playback speed: ' + str(replay_fps) + 'fps, spike_threshold: ' + str(self.spike_threshold))


        # self.ax.set_xlim([0,10])
        # self.ax.set_ylim([-2,2])
        interval = 1000 / replay_fps  # in ms
        self.anim = animation.FuncAnimation(self.fig, self.update_spiking_replay_motion,frames=self.Vis.N_frames, interval=2, repeat=False,
                                            blit=True)
        #plt.tight_layout()
        plt.show()

    def update_spiking_replay_motion(self, i):

        if i ==0:
            self.prev_spike_matrices = []
            for i in range(self.reichardt_delay):
                self.prev_spike_matrices.append(np.ones((self.Vis.NROWS, self.Vis.NCOLS)).astype('int8'))


        if i < self.Vis.N_frames:
            self.im_an.set_array(self.data[i])

            spike_matrix = TouchSensor.analog2diff(self.data[i], self.data[i - 1],
                                                   self.spike_threshold)  # self.make_spike_matrix(i)

            spike_matrices_motion = []
            motions = ['D2U','R2L','L2R','U2D']
            prev_spike_matrix = self.prev_spike_matrices.pop(0)
            for motion in motions:
                if self.motion_type == 'doubleEdge':
                    spike_matrices_motion.append(TouchSensor.diff2motion(spike_matrix,motion))
                elif self.motion_type == 'Reichardt':
                    spike_matrices_motion.append(
                        TouchSensor.diff2motion_reichardt(spike_matrix, prev_spike_matrix, motion,
                                                          self.reichardt_delay))

            # print self.data[i]
            # print spike_matrix
            # print prev_spike_matrix
            # print spike_matrices_motion
            print 'frame ' + str(i) + '/' + str(self.Vis.N_frames)

            self.im_sp.set_array(spike_matrix)
            for idx, imshow in enumerate(self.motion_imshows):
                imshow.set_array(spike_matrices_motion[idx])

        else:
            # self.ax_spike.set_xlabel('frame ' + str(self.N_frames) + '/' + str(self.N_frames))
            pass

        self.prev_spike_matrices.append(spike_matrix)
        return self.im_an, self.im_sp,self.motion_imshows[0],self.motion_imshows[1],self.motion_imshows[2],self.motion_imshows[3],#self.im_sp_up,self.im_sp_left,self.im_sp_right,self.im_sp_down


    def spiking_motion_live(self, motion_type='doubleEdge'):


        try:
            self.Vis = TouchSensor.Visualiser()

        except IOError as e:
            print 'uC not found'
            print e.args
            if e.args[0] == 16:
                self.print_status(e.args[1] + ', please try again')
            else:
                self.print_status(e.args[0] + ', please try again')
            return

        except serial.serialutil.SerialException as e:
            print 'uC found busy'
            self.print_status(e.args[1]+', please try again')
            return

        self.motion_type = motion_type
        self.reichardt_delay=1

        # read input boxes
        try:
            replay_fps = int(self.win.viz_butt_lay.replay_fps.text())
        except:
            replay_fps = 20
        try:
            self.spike_threshold = int(self.win.viz_butt_lay.spike_threshold.text())
        except:
            self.spike_threshold = 1

        # get data
        #dataFile = self.win.sel_exp  # '/home/alexander/Documents/ExperimentData/FlexSensor/2018-12-17-17-14-15_test_flexSensor_dataSave'

        #self.data = self.Vis.data  # np.array(self.Vis.data).reshape(-1, 5, 5)  # .astype(numpy.float32)
        #self.data[0][0][0] = 50


        #rec_dur = self.Vis.timestamps[-1][0]
        #recording_FR = self.data.shape[0]/rec_dur




        self.fig = plt.figure(figsize=(9,12))
        gs = gridspec.GridSpec(nrows=5, ncols=3, height_ratios=[2,2,2,2,2])

        #analog axis
        frame_init = np.zeros((self.Vis.NROWS, self.Vis.NCOLS)).astype('uint8')
        frame_init[0][0] = 50

        ax_analog = self.fig.add_subplot(gs[0,1])  # , projection='3d')
        ax_analog.set_title('analog mode')
        self.im_an = ax_analog.imshow(frame_init, cmap=cm.jet, interpolation='none', animated=True)
        ax_analog.add_artist(self.im_an)
        cbar_analog = self.fig.colorbar(self.im_an)

        #diff spiking axis
        ax_spiking = self.fig.add_subplot(gs[1,1])  # create an axis
        ax_spiking.set_title('diff spiking mode')
        # self.ax_spiking.hold(False)  # discards the old graph
        spike_matrix_init = np.zeros((self.Vis.NROWS, self.Vis.NCOLS)).astype('uint8')
        spike_matrix_init[0][0] = 2
        cmap = mpl.colors.ListedColormap(['black', 'gray', 'white'])
        self.im_sp = ax_spiking.imshow(spike_matrix_init, cmap=cmap, interpolation='none', animated=True)
        ax_spiking.add_artist(self.im_sp)
        #cbar_spiking = self.fig.colorbar(self.im_sp)
        cbar_spiking = self.fig.colorbar(self.im_sp,ticks=[0,1,2])
        cbar_spiking.ax.set_yticklabels(['-', '0', '+'])



        spike_matrix_init = np.zeros((self.Vis.NROWS, self.Vis.NCOLS)).astype('uint8')
        spike_matrix_init[0][0] = 2
        cmap = mpl.colors.ListedColormap(['black','gray', 'white'])
        locs = [[2,1],[3,0],[3,2],[4,1]]
        self.motion_imshows = []

        for idx, itm in enumerate(['up','left','right','down']):
            # motion spiking axis
            ax = self.fig.add_subplot(gs[locs[idx][0], locs[idx][1]])  # create an axis
            ax.set_title(itm)
            self.motion_imshows.append(ax.imshow(spike_matrix_init, cmap=cmap, interpolation='none', animated=True))
            ax.add_artist(self.motion_imshows[-1])
            cbar_motion = self.fig.colorbar(self.motion_imshows[-1], ticks=[0, 1])
            cbar_motion.ax.set_yticklabels(['dead','0', '+'])

        #plt.suptitle(
        #    'Recording: '+str(self.Vis.N_frames) + ' frames in ' + str(rec_dur) + 's (' + str(recording_FR) + 'Hz)'
        #    + ', playback speed: ' + str(replay_fps) + 'fps, spike_threshold: ' + str(self.spike_threshold))

        plt.suptitle('spike_threshold: ' + str(self.spike_threshold))


        # self.ax.set_xlim([0,10])
        # self.ax.set_ylim([-2,2])
        interval = 1000 / 46 #replay_fps  # in ms , interval=interval
        self.anim = animation.FuncAnimation(self.fig, self.update_spiking_motion_live, interval=interval, repeat=False,
                                            blit=True)
        #plt.tight_layout()
        plt.show()

    def update_spiking_motion_live(self, i):

        if i ==0:
            self.prev_spike_matrices = []
            for i in range(self.reichardt_delay):
                self.prev_spike_matrices.append(np.ones((self.Vis.NROWS, self.Vis.NCOLS)).astype('int8'))
            self.prev_frame = np.zeros((self.Vis.NROWS, self.Vis.NCOLS)).astype('int8')

        frame = self.Vis.Ser.read_frame()

        self.im_an.set_array(frame)

        spike_matrix = TouchSensor.analog2diff(frame, self.prev_frame,
                                               self.spike_threshold)  # self.make_spike_matrix(i)

        spike_matrices_motion = []
        motions = ['D2U','R2L','L2R','U2D']
        prev_spike_matrix = self.prev_spike_matrices.pop(0)
        for motion in motions:
            if self.motion_type == 'doubleEdge':
                spike_matrices_motion.append(TouchSensor.diff2motion(spike_matrix,motion))
            elif self.motion_type == 'Reichardt':
                spike_matrices_motion.append(
                    TouchSensor.diff2motion_reichardt(spike_matrix, prev_spike_matrix, motion,
                                                      self.reichardt_delay))

        print frame
        print spike_matrix
        print prev_spike_matrix
        print spike_matrices_motion

        self.im_sp.set_array(spike_matrix)
        for idx, imshow in enumerate(self.motion_imshows):
            imshow.set_array(spike_matrices_motion[idx])

        self.prev_frame = frame
        self.prev_spike_matrices.append(spike_matrix)
        return self.im_an, self.im_sp,self.motion_imshows[0],self.motion_imshows[1],self.motion_imshows[2],self.motion_imshows[3],#self.im_sp_up,self.im_sp_left,self.im_sp_right,self.im_sp_down


    # def update_spiking_replay_motion_reichardt(self, i):
    #
    #     if i ==0:
    #         self.prev_spike_matrices = []
    #         for i in range(self.reichardt_delay):
    #             self.prev_spike_matrices.append(np.ones((self.Vis.NCOLS, self.Vis.NCOLS)).astype('int8'))
    #
    #
    #     if i < self.Vis.N_frames:
    #         self.im_an.set_array(self.data[i])
    #
    #         spike_matrix = TouchSensor.analog2diff(self.data[i], self.data[i - 1],
    #                                                self.spike_threshold)  # self.make_spike_matrix(i)
    #
    #         spike_matrices_motion = []
    #         motions = ['D2U','R2L','L2R','U2D']
    #         prev_spike_matrix = self.prev_spike_matrices.pop(0)
    #         for motion in motions:
    #             spike_matrices_motion.append(TouchSensor.diff2motion_reichardt(spike_matrix,prev_spike_matrix,motion,self.reichardt_delay))
    #
    #         print self.data[i]
    #         print spike_matrix
    #         print prev_spike_matrix
    #         print spike_matrices_motion
    #
    #         self.im_sp.set_array(spike_matrix)
    #         for idx, imshow in enumerate(self.motion_imshows):
    #             imshow.set_array(spike_matrices_motion[idx])
    #
    #     else:
    #         # self.ax_spike.set_xlabel('frame ' + str(self.N_frames) + '/' + str(self.N_frames))
    #         pass
    #
    #     self.prev_spike_matrices.append(spike_matrix)
    #     return self.im_an, self.im_sp,self.motion_imshows[0],self.motion_imshows[1],self.motion_imshows[2],self.motion_imshows[3],#self.im_sp_up,self.im_sp_left,self.im_sp_right,self.im_sp_down



    def simple_animation(self):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)  # create an axis
        self.ax.hold(False)  # discards the old graph
        self.circle = Circle((0,0), 1.0)
        self.ax.add_artist(self.circle)
        self.ax.set_xlim([0,10])
        self.ax.set_ylim([-2,2])

        self.anim = animation.FuncAnimation(self.fig,self.animate_loop,frames=10,interval=100,repeat=False,blit=True)
        plt.show()

    def animate_loop(self, i):
        self.circle.center = (i, 0)
        return self.circle,


    def live_feed(self):


        try:
            Vis = TouchSensor.Visualiser()

        except IOError as e:
            print 'uC not found'
            print e.args
            if e.args[0] == 16:
                self.print_status(e.args[1] + ', please try again')
            else:
                self.print_status(e.args[0] + ', please try again')
            return


        Vis.animate_live()
        plt.show()
        # Ser = TouchSensor.SerialListener()
        # for i in range(100):
        #     data_matrix =  Ser.read_frame()
        return


    def plot_mean_activity(self):
        print 'plot mean activity'
        datafile = self.win.sel_exp
        Pr = TouchSensor.Processor(datafile)
        Pr.plot_mean_distance()

        return

    def plot_mean_column_activity(self):
        print 'plot mean column activity'
        datafile = self.win.sel_exp
        Pr = TouchSensor.Processor(datafile)
        Pr.plot_meanColumn_distance()

        return

    def plot_mean_activity_spikes(self,histogram=False):
        print 'plot mean activity with spikes'
        datafile = self.win.sel_exp
        Pr = TouchSensor.Processor(datafile)
        try:
            self.spike_threshold = int(self.win.viz_butt_lay.spike_threshold.text())
        except:
            self.spike_threshold = 1
        try:
            frames_to_skip = int(self.win.viz_butt_lay.frames_to_skip.text())
        except:
            frames_to_skip = 0

        Pr.plot_mean_with_histogram(spike_thresholds=[int(self.spike_threshold/2),self.spike_threshold,self.spike_threshold*2],spike_frames_to_skip=frames_to_skip,histogram=histogram)

        return

    def plot_distance_and_servo(self):
        datafile = self.win.sel_exp
        Pr = TouchSensor.Processor(datafile)
        Pr.plot_distance_data()
        return

    def plot_all_traces(self):
        inverse = self.win.viz_butt_lay.b12b.isChecked()
        datafile = self.win.sel_exp
        Pr = TouchSensor.Processor(datafile)
        Pr.plot_all_traces(inverseRowCol=inverse)
        return

    def performExperiment(self, name):
        if name == "Start Recording":
            self.start_recording()
        if name == "Stop Recording":
            self.stop_recording()
        return

    def start_recording(self):
        try:
            rec_dur = int(self.win.exp_butt_lay.rec_dur.text())
        except:
            self.print_status('Please fill in integer recording time in seconds')
            return

        exp_name = self.win.exp_butt_lay.exp_name.text()

        self.print_status('Recording ongoing for '+str(rec_dur)+'s ...')
        try:
            Ser = TouchSensor.SerialListener()
        except IOError:

            self.print_status('no uC found')
            return
        Ser.save_data(exp_name, rec_dur=rec_dur)
        self.print_status('Recording finished')
        self.win.exp_list_lay.loadExperiments()
        return

    def stop_recording(self):
        self.print_status('Not implemented')
        return

    def print_status(self, text):
        # text = date.strftime("Exp %d/%m/%Y - %H:%M:%S") + ' - ' +
        self.item.setText(text)
        #item.setData(QtCore.Qt.UserRole, dirname + "/" + filename)
        self.update()
        self.win.repaint()
        return



    def dataSendLoop(self, addData_callbackFunc):
        # Setup the signal-slot mechanism.
        mySrc = Communicate()
        mySrc.data_signal.connect(addData_callbackFunc)

        # Simulate some data
        n = np.linspace(0, 499, 500)
        y = 50 + 25 * (np.sin(n / 8.3)) + 10 * (np.sin(n / 7.5)) - 5 * (np.sin(n / 1.5))
        i = 0

        while (True):
            if (i > 499):
                i = 0
            time.sleep(0.1)
            mySrc.data_signal.emit(y[i])  # <- Here you emit a signal!
            i += 1
            ###
        return

    def test_animation(self):
        self.myFig = CustomFigCanvas()
        self.addWidget(self.myFig, *(0,1))

        myDataLoop = threading.Thread(name = 'myTestdataLoop', target = testdataSendLoop, args = (self.addData_callbackFunc,))
        myDataLoop.daemon = True
        myDataLoop.start()

        #self.show()

    def addData_callbackFunc(self, value):
        # print("Add data: " + str(value))
        self.myFig.addData(value)

    def plotSensor(self, name):

        self.clean()
        data_init = self.win.sel_conf[0]
        data = self.win.sel_conf[self.win.sel_ite]
        l = name.split()[-1]

        # Multi lines
        if len(data_init[l.lower() + "_rob"].shape) > 1:

            self.plot = SimpleFigure(subplot=211)
            self.addWidget(self.plot)
            self.plot.axes.cla()
            rob_mean, sim_mean = utils.center_norm_2(np.mean(data_init[l.lower() + "_rob"], axis=1),
                                                     np.mean(data[l.lower() + "_sim"], axis=1))
            self.plot.axes.plot(data_init["t"], data_init[l.lower() + "_rob"], linewidth=0.4, color="skyblue")
            self.plot.axes.plot(data_init["t"], data[l.lower() + "_sim"], linewidth=0.8, color="orchid")
            self.plot.axes.set_xlim([data_init["t"][0], data_init["t"][-1]])

            self.plot.axes2 = self.plot.fig.add_subplot(212)
            self.plot.axes2.plot(data_init["t"], rob_mean, linewidth=2.5, color=self.getStyleColors()[0],
                                 label="Averaged Centered " + l + " Rob")
            self.plot.axes2.plot(data_init["t"], sim_mean, linewidth=2.5, color=self.getStyleColors()[1],
                                 label="Averaged Centered " + l + " Sim")
            self.plot.axes2.fill_between(data_init["t"], np.abs(sim_mean - rob_mean), alpha=0.3,
                                         edgecolor=self.getStyleColors()[2], facecolor=self.getStyleColors()[2],
                                         label="Absolute Error")
            self.plot.axes2.set_xlim([data_init["t"][0], data_init["t"][-1]])
            self.plot.axes.legend(loc='best', fontsize="x-small")
            # self.plot.axes.set_title("Periodic average of the robot under-actuated front left leg sensor", fontsize=14)
            # self.plot.axes.set_ylabel('Leg knee angle (degrees)')
            # self.plot.axes.set_xlabel('Time (s)')

        else:
            self.plot = SimpleFigure()
            self.addWidget(self.plot)
            self.plot.axes.cla()
            self.plot.axes.plot(data_init["t"], data_init[l.lower() + "_rob"], linewidth=1, label=l + " Leg Robot")
            self.plot.axes.plot(data_init["t"], data[l.lower() + "_sim"], linewidth=1, label=l + " Leg Simulation")
            self.plot.axes.legend()

        self.plot.draw()
        # self.plot.save("average.png")

    def plotEvolution(self):

        self.clean()
        scores = [t["score"] for t in self.win.sel_conf]
        pop_size = self.win.sel_conf[0]["pop"]
        if pop_size == 0:
            print 'woops, saved popsize 0, will assume popsize 10...'
            pop_size = 10
        try:
            x, y_min, y_max, y_av = self.rearrangePop(scores, pop_size)
        except AssertionError as e:
            print e
            return

        self.plot = SimpleFigure()
        self.addWidget(self.plot)

        self.plot.axes.cla()
        self.plot.axes.plot(x, y_max, linestyle="-", color=self.getStyleColors()[1], linewidth=1,
                            label="Generation Maximum")
        self.plot.axes.plot(x, y_av, linestyle="-", color=self.getStyleColors()[3], linewidth=1,
                            label="Generation Average")
        self.plot.axes.plot(x, y_min, linestyle="-", color=self.getStyleColors()[0], linewidth=1,
                            label="Generation Minimum")
        self.plot.axes.set_title("Training score of CMA-ES algorithm with popSize = " + str(pop_size), fontsize=14)
        self.plot.axes.set_ylabel('Sensor Error')
        self.plot.axes.set_xlabel('Generation Epoch')
        self.plot.axes.legend(loc="upper right", fontsize="small")
        self.plot.axes.xaxis.label.set_color('white')
        self.plot.axes.yaxis.label.set_color('white')
        self.plot.axes.title.set_color('white')
        # self.plot.save("cma_results.png")

    def plotParams(self, n=10):

        self.clean()

        t = range(0, len(self.win.sel_conf))
        params = np.array([i["params"] for i in self.win.sel_conf])[:, -n:].T
        labels = np.array(self.win.sel_conf[0]["params_names"])[-n:]

        self.plot = SimpleFigure()
        self.addWidget(self.plot)

        for y_arr, label in zip(params, labels):
            self.plot.axes.plot(t, y_arr, linewidth=0.8, label=label)

        self.plot.axes.set_title("Evolution of Optimized Parameters", fontsize=14)
        self.plot.axes.set_ylabel('Normalized parameters')
        self.plot.axes.set_xlabel('Generation Epoch')
        self.plot.axes.legend(loc="upper left", fontsize="small")
        self.plot.axes.xaxis.label.set_color('white')
        self.plot.axes.yaxis.label.set_color('white')
        self.plot.axes.title.set_color('white')
        # self.plot.save("cma_results.png")

    def showSimParams(self):

        self.clean()

        params_unormed = utils.unorm(self.win.sel_conf[self.win.sel_ite]["params"],
                                     self.win.sel_conf[0]["params_min"],
                                     self.win.sel_conf[0]["params_max"])
        params_names = self.win.sel_conf[0]["params_names"]
        params_units = self.win.sel_conf[0]["params_units"]
        data = {k: self.win.sel_conf[self.win.sel_ite][k]
                for k in ('iter', "score", "params", "elapsed time") if k in self.win.sel_conf[self.win.sel_ite]}

        for i, p in enumerate(params_unormed):
            data[params_names[i]] = "{0:.4f} ".format(p) + str(params_units[i])

        self.table = SimpleTable(data, len(data), 2)
        self.addWidget(self.table)

        return

    def showOptimParams(self):

        self.clean()

        scores = [self.win.sel_conf[i]["score"] for i in range(len(self.win.sel_conf))]
        data = {k: self.win.sel_conf[0][k] for k in ('bag_file', "sim_file", "sim_time", "start_time", "stop_time",
                                                     'eval_points', 'start_eval_time', 'stop_eval_time', 'pool_number',
                                                     'init_var', 'min', 'max', 'pop', 'max_iter', 'score_method',
                                                     'params_min', 'params_max', 'params_names', 'params_units')
                if k in self.win.sel_conf[0]}
        data["best_score"] = min(scores)
        data["best_iteration"] = scores.index(min(scores)) + 1
        self.table = SimpleTable(data, len(data), 2)
        self.addWidget(self.table)

        return

    def simulate(self, current=True):

        if current:
            sim_id = self.win.sel_ite
        else:
            scores = [self.win.sel_conf[i]["score"] for i in range(len(self.win.sel_conf))]
            sim_id = scores.index(min(scores))
        s = SimpleSimulation(win=self.win, sim_id=sim_id, time_step=0.02)
        s.simulate()
        del s

    def generateModel(self, current=True):
        sim_id = self.win.sel_ite

        data = self.win.sel_conf[sim_id]
        # data_init = self.win.sel_conf[0]
        self.config = data["config"]
        self.file = "/home/alexander/.gazebo/models/tigrillo/model.sdf"  # data_init["model_file"]
        # Create the model
        fg = model.SDFileGenerator(self.config, self.file, model_scale=1, gazebo=True)
        fg.generate()

    def clean(self):

        for i in reversed(range(self.count())):
            self.itemAt(i).widget().setParent(None)

    def getStyleColors(self):

        if 'axes.prop_cycle' in plt.rcParams:
            cols = [p['color'] for p in plt.rcParams['axes.prop_cycle']]
        else:
            cols = ['b', 'r', 'y', 'g', 'k']
        return cols

    def rearrangePop(self, scores, ps):

        array = np.array(scores)

        array = array[0:len(scores) - (len(scores) % ps)]
        # assert len(scores) % ps == 0, "The total number of iteration (" + str(len(scores)) + \
        #                                ") shall be a multiple of the population size (" + \
        #                                str(ps) + "). Please verify the file " +  \
        #                                self.win.sel_exp + " or this sript!"

        matrix = np.reshape(array, (-1, ps))
        y_min = np.min(matrix, axis=1)
        y_max = np.max(matrix, axis=1)
        y_av = np.mean(matrix, axis=1)
        x = np.array(range((y_av.size))) + 1
        return x, y_min, y_max, y_av

class mplWidget(FigureCanvas):
    def __init__(self):
        super(mplWidget, self).__init__(mpl.figure.Figure(figsize=(7, 7)))

        self.setupAnim()
        self.show()

    def setupAnim(self):
        ax = self.figure.add_axes([0, 0, 1, 1], frameon=False)
        ax.axis([0, 1, 0, 1])
        ax.axis('off')

        data_file_dir = '/home/kiwi/Documents/ExperimentData/FlexSensor/2019-2-25-18-12-7_kiwi_palpitations'
        # data_file_dir = '/home/alexander/Documents/ExperimentData/FlexSensor/2019-3-5-16-44-8_thumb_LR'
        data_file = tables.open_file(data_file_dir, mode='r', title='ReadData')
        self.data = data_file.root.data
        self.Z = np.array(self.data).reshape(-1, 5, 5)  # .astype(numpy.float32)

        self.Z[0][0][0] = 50
        # Construct the scatter which we will update during animation
        # as the raindrops develop.
        self.im = ax.imshow(self.Z[0], cmap=cm.jet, interpolation='none', animated=True)

        self.animation = animation.FuncAnimation(self.figure, self.update_plot,
                                       interval=10, blit=True)

    def update_plot(self, i):


        print self.Z[i]
        self.im.set_array(self.Z[i])
        return self.im,

class SimpleFigure(FigureCanvas):

    def __init__(self, parent=None, subplot=111, width=8, height=6, dpi=100):

        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.fig.patch.set_facecolor("None")
        self.axes = self.fig.add_subplot(subplot)

        FigureCanvas.__init__(self, self.fig)

        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.setStyleSheet("background-color:transparent;")
        self.updateGeometry()

    def save(self, name="figure.png"):

        self.axes.xaxis.label.set_color('black')
        self.axes.yaxis.label.set_color('black')
        self.axes.title.set_color('black')
        self.axes.tick_params(axis='x', colors='black')
        self.axes.tick_params(axis='y', colors='black')
        self.fig.set_size_inches(7, 5)
        self.fig.savefig(name, format='png', dpi=300)
        self.axes.xaxis.label.set_color('white')
        self.axes.yaxis.label.set_color('white')
        self.axes.title.set_color('white')
        self.axes.tick_params(axis='x', colors='white')
        self.axes.tick_params(axis='y', colors='white')


def gui():

    RESULT_FOLDER = os.environ['HOME']+'/Documents/ExperimentData/FlexSensor'
    #RESULT_FOLDER = '/home/kiwi/Dropbox/UGent/ExperimentData/Flexsensor'#'/home/kiwi/Dropbox/UGent/Files/FlexSensor'
    #RESULT_FOLDER = os.environ['HOME']+'/Dropbox/UGent/ExperimentData/Flexsensor'
    app = QtWidgets.QApplication(sys.argv)
    win = AppWin(RESULT_FOLDER)

    # Dark style
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    sys.exit(app.exec_())


if __name__ == '__main__':

     gui()