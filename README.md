## Touch sensor

the sensor can be produced in various designs and with various materials. A piezoresistive material (e.g. Velostat or Eeontex fabric) is used as it's resistance changes if pressure is applied. The resistance is read out with a microcontroller using a simple voltage divider circuit.

![picture](doc/voltDiv.png)

To have multiple sensing points, a matrix can be created using crossing wires, with 'rows' and 'columns' on opposite sides of the piezoresistive material. The matrix is then read out one ADC at the time and one column at the time.

![picture](doc/matrix.png)

## Hardware

Documentation on sensor design can be found in [/doc](https://bitbucket.org/alex_vds_ugent/hbp_touchsensor/src/master/doc/).  

## Code

The sensor is read out with a microcontroller, example code can be found in [/src/V2/MASTER](https://bitbucket.org/alex_vds_ugent/hbp_touchsensor/src/master/src/V2/MASTER).

For recording and visualising data, code is available in [/src/PROCESS/TouchSensor.py](https://bitbucket.org/alex_vds_ugent/hbp_touchsensor/src/master/src/PROCESS/TouchSensor.py) . If desired, a GUI is also available ([/src/PROCESS/GUI_TouchSensor.py](https://bitbucket.org/alex_vds_ugent/hbp_touchsensor/src/master/src/PROCESS/GUI_TouchSensor.py)).
