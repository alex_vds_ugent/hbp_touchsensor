This sensor is fabricated using Velostat and Kapton tape.

The dimensions of the sensor and the density can be adapted to suit the project.

[Video instructions](https://drive.google.com/file/d/1pTGj1fr1gQl8wW3HBNNRoA-TZ1sgk7zV/view) on making a sensor are available.
